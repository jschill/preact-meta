import { Component } from 'preact';
interface IPreactMeta {
    name: string;
    content: string;
}
declare class PreactMeta extends Component<IPreactMeta, null> {
    removeMeta: () => void;
    addMeta: () => void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): null;
}
export default PreactMeta;
