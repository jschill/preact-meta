"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var preact_1 = require("preact");
var PreactMeta = /** @class */ (function (_super) {
    __extends(PreactMeta, _super);
    function PreactMeta() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.removeMeta = function () {
            var name = _this.props.name;
            var meta = document.querySelector('meta[name=' + name + ']');
            if (meta && meta.parentNode) {
                meta.parentNode.removeChild(meta);
            }
        };
        _this.addMeta = function () {
            _this.removeMeta();
            var metaElement = document.createElement('meta');
            metaElement.setAttribute('name', _this.props.name);
            metaElement.content = _this.props.content;
            document.getElementsByTagName('head')[0].appendChild(metaElement);
        };
        return _this;
    }
    PreactMeta.prototype.componentDidMount = function () {
        this.addMeta();
    };
    PreactMeta.prototype.componentWillUnmount = function () {
        this.removeMeta();
    };
    PreactMeta.prototype.render = function () {
        return null;
    };
    return PreactMeta;
}(preact_1.Component));
exports.default = PreactMeta;
