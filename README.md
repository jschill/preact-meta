# preact-meta

Dead simple declarative handling of meta tags in Preact

## Usage

```javascript
import Meta from 'preact-meta';

...

<Meta name="description" content="Page meta description" />
```

## Caveats
Do not nest :-)