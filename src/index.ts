﻿import { Component } from 'preact';

interface IPreactMeta {
    name: string;
    content: string;
}

class PreactMeta extends Component<IPreactMeta, null> {
    removeMeta = () => {
        const { name } = this.props;
        const meta = document.querySelector('meta[name=' + name + ']');
        if (meta && meta.parentNode) {
            meta.parentNode.removeChild(meta);
        }
    }

    addMeta = () => {
        this.removeMeta();
        const metaElement = document.createElement('meta');
        metaElement.setAttribute('name', this.props.name);
        metaElement.content = this.props.content;
        document.getElementsByTagName('head')[0].appendChild(metaElement);
    }

    componentDidMount () {
        this.addMeta();
    }

    componentWillUnmount () {
        this.removeMeta();
    }

    render () {
        return null;
    }
}

export default PreactMeta;
